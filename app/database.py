import pymongo

class Database:
    
    def __init__(self):
        # MongoDB URI
        uri = "mongodb+srv://unishare:unishare@mathapi.dg9us.azure.mongodb.net/mathapi?retryWrites=true&w=majority"
        # Connect to MongoDB
        self.client = pymongo.MongoClient(uri)
        # Select DB
        self.db = self.client['mathapi_db']
        # Select connection
        self.collection = self.db['mathapi_collection']

    def get_client(self):
        return self.client

    def get_db(self):
        return self.db

    def get_collection(self):
        return self.collection

    def add(self, op, p1, p2, result):
        # Create row
        row = {
            "op": op,
            "p1": p1,
            "p2": p2,
            "result": result
        }
        # Push row
        row_id = self.collection.insert_one(row).inserted_id
        # Return row id
        return row_id

    def history(self):
        
        history = []
        for item in self.collection.find():
            
            print(item)

            if item['op'] == 'add':
                history.append(str(item['p1']) + ' + ' + str(item['p2']) + ' = ' + str(item['result']))
            elif item['op'] == 'sub':
                history.append(str(item['p1']) + ' - ' + str(item['p2']) + ' = ' + str(item['result']))
            elif item['op'] == 'mul':
                history.append(str(item['p1']) + ' * ' + str(item['p2']) + ' = ' + str(item['result']))
            elif item['op'] == 'div':
                history.append(str(item['p1']) + ' / ' + str(item['p2']) + ' = ' + str(item['result']))
            else:
                history.append('error')
            
        return history