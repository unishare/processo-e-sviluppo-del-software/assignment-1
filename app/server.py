from operations import Add, Sub, Mul, Div
from flask import Flask, json
from database import Database
import os

api = Flask(__name__)
db = Database()

@api.route('/', methods=['GET'])
def get_main():
    res = {
        "status": "success",
        "error": False,
        "operations": [
            "/add",
            "/sub",
            "/mul",
            "/div",
            "/history",
            "/authors"        
        ]
    }

    return json.dumps(res)


@api.route('/add/<p1>/<p2>', methods=['GET'])
def get_add(p1 = None, p2 = None):
    global db

    result = Add.add(p1, p2)

    res = {
        "status": "success",
        "error": False,
        "response": {
            "p1": p1,
            "p2": p2,
            "result": result
        }
    }

    db.add('add', p1, p2, result)

    return json.dumps(res)


@api.route('/sub/<p1>/<p2>', methods=['GET'])
def get_sub(p1 = None, p2 = None):
    global db

    result = Sub.sub(p1, p2)

    res = {
        "status": "success",
        "error": False,
        "response": {
            "p1": p1,
            "p2": p2,
            "result": result
        }
    }

    db.add('sub', p1, p2, result)

    return json.dumps(res)

@api.route('/mul/<p1>/<p2>', methods=['GET'])
def get_mul(p1 = None, p2 = None):
    global db

    result = Mul.mul(p1, p2)

    res = {
        "status": "success",
        "error": False,
        "response": {
            "p1": p1,
            "p2": p2,
            "result": result
        }
    }

    db.add('mul', p1, p2, result)

    return json.dumps(res)


@api.route('/div/<p1>/<p2>', methods=['GET'])
def get_div(p1 = None, p2 = None):
    global db

    result = Div.div(p1, p2)

    res = {
        "status": "success",
        "error": False,
        "response": {
            "p1": p1,
            "p2": p2,
            "result": result
        }
    }

    db.add('div', p1, p2, result)

    return json.dumps(res)


@api.route('/history', methods=['GET'])
def get_history():
    global db

    history = db.history()

    res = {
        "status": "success",
        "error": False,
        "history": history
    }

    return json.dumps(res)


@api.route('/authors', methods=['GET'])
def get_authors():
    res = {
        "status": "success",
        "error": False,
        "authors": [
            "Davide Cozzi",
            "Gabriele De Rosa",
            "Federica Di Lauro"
        ]
    }

    return json.dumps(res)


port_os = os.getenv('PORT', default='8080')
if __name__ == '__main__':
    api.run(host='0.0.0.0', port=port_os)