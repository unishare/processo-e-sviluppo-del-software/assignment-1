import math

class Utils:

    @staticmethod
    def cast(p):
        if p is None:
            return None
        return float(p)

class Add:

    @staticmethod
    def add(p1, p2):
        # Cast
        p1 = Utils.cast(p1)
        p2 = Utils.cast(p2)
        # Calculate
        if p1 and p2:
            result = p1 + p2
        elif p1 and not p2:
            result = p1
        else:
            result = None
        # Return
        return result

class Sub:

    @staticmethod
    def sub(p1, p2):
        # Cast
        p1 = Utils.cast(p1)
        p2 = Utils.cast(p2)
        # Calculate
        if p1 and p2:
            result = p1 - p2
        elif p1 and not p2:
            result = p1
        else:
            result = None
        # Return
        return result

class Mul:

    @staticmethod
    def mul(p1, p2):
        # Cast
        p1 = Utils.cast(p1)
        p2 = Utils.cast(p2)
        # Calculate
        if p1 and p2:
            result = p1 * p2
        elif p1 and not p2:
            result = p1
        else:
            result = None
        # Return
        return result


class Div:
    
    @staticmethod
    def div(p1, p2):
        # Cast
        p1 = Utils.cast(p1)
        p2 = Utils.cast(p2)
        # Calculate
        if p2 == 0:
            result = math.inf
        elif p1 == 0:
            result = float(0)
        elif p1 and p2:
            result = p1 / p2
        elif p1 and not p2:
            result = p1
        else:
            result = None
        # Return
        return result
