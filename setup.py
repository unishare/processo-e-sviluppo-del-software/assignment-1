import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="math-api",
    version="1.0.0",
    author="derogab, dlcgold, fdila",
    description="API for simple operations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/unishare/processo-e-sviluppo-del-software/assignment-1/",
    license='MIT',
    package_dir={'math_api': 'app'},
    packages=[
        'app'
    ],
    install_requires=[
        'Flask',
        'pandas'
    ],
    keywords='math-api',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3',
)