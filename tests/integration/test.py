# Tools
import pytest
import sys
import math
import os
import requests

# Magic
sys.path.append("./app/")

# App
from operations import Add, Sub, Mul, Div
from database import Database 
db = Database()

# Tests
def test_exist():
    global db
    collections = db.get_db().list_collection_names()
    assert 'mathapi_collection' in collections

def test_add():
    global db
    # Add
    add_id = db.add('div', 8.0, 2.0, 4.0)
    assert add_id != None
    # Check
    query = db.get_collection().find_one({"_id": add_id})
    check = {
        "_id": add_id,
        "op": 'div',
        "p1": 8.0,
        "p2": 2.0,
        "result": 4.0
    }
    assert check == query
    # Remove
    db.get_collection().delete_one({"_id": add_id})
    query_remove = db.get_collection().find_one({"_id": add_id})
    assert query_remove == None

def test_api():
    res = requests.get('http://localhost:8080/authors')
    assert res.status_code == 200
    assert res.text == '{"authors": ["Davide Cozzi", "Gabriele De Rosa", "Federica Di Lauro"], "error": false, "status": "success"}'