# Tools
import pytest
import sys
import math

# Magic
sys.path.append("./app/")

# App
from operations import Add, Sub, Mul, Div

# Tests
def test_add():
    assert 2+2 == Add.add(2, 2)

def test_sub():
    assert 2-2 == Sub.sub(2, 2)

def test_mul():
    assert 2 == Mul.mul(2, None)

def test_div():
    assert math.inf == Div.div(2, 0)