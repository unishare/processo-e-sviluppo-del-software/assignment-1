#!/bin/bash

set -xe

echo "$SSH_PRIVATE_KEY_AZURE" > .gitlab-ci-deploy-azure-private-key.pem
chmod 400 .gitlab-ci-deploy-azure-private-key.pem
ssh -o 'StrictHostKeyChecking no' -i .gitlab-ci-deploy-azure-private-key.pem $AZURE_USER@$AZURE_IP \
"docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY \
&& docker stop math-api || true \
&& docker rm --force math-api || true \
&& docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME \
&& docker run -d --restart on-failure:5 --name math-api -p 80:8080 $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME \
&& exit"

