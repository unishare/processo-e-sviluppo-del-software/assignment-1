# Assignment 1

## Repository link
- [https://gitlab.com/fdila/2020_assignment1_pypeline](https://gitlab.com/fdila/2020_assignment1_pypeline/)

## Autori
- 829470 Federica Di Lauro
- 829827 Davide Cozzi 
- 829835 Gabriele De Rosa


## Breve descrizione app

L'applicazione è una piccola Web App scritta in linguaggio Python mediante
l'utilizzo del framework Flask. Si tratta di una API REST in grado di eseguire 
addizioni, sottrazioni, moltiplicazioni e divisioni. Inoltre è presente una 
cronologia delle operazioni eseguite che viene salvata su un database remoto 
`MongoDB`.

[Documentazione API](https://app.swaggerhub.com/apis/fdila/math-api/1.0.0)

## Pipeline

È stata utilizzata come base per la pipeline l'immagine docker ufficiale di 
Python. In seguito alla necessità di avere a disposizione le medesime dipendenze 
in alcuni job, è stato abilitato un sistema di cache sulla pipeline. 
Mediante un `before_script` globale viene quindi definito un ambiente 
`virtualenv` che sarà poi riutilizzato (dove serve) durante l'intera pipeline.

Per la cache è stata scelta una key che permette l'utilizzo della stessa
sia tra job che tra branch diversi in modo da non dover riscaricare 
ogni volta le dipendenze utili all'app e le dipendenze necessarie per 
l'esecuzione dei test.

Nei 3 stages finali la cache è disabilitata in quanto non si ha più 
la necessità di installare le suddette dipendenze.

#### Build

L'installazione delle dipendenze è verificata utilizzando `pip`. 
Un elenco di tutte le dipendenze necessarie è contenuto nel file 
`requirements.txt`.  
Nel dettaglio:

- `flask`, necessario per il web-server
- `pymongo`, necessario per la connessione al database
- `dnspython`, necessario a `pymongo`

Si specifica che queste dipendenze sono relative unicamente all'applicativo e 
non ai tool necessari alla svolgimento della pipeline.

Questo stage viene eseguito su tutti i branch.

#### Verify

Sono utilizzati i tool `Bandit` e `Prospector` per l'analisi del codice, 
entrambi installati appositamente per il relativo job.
I log derivanti dall'esecuzione di questi tool sono quindi salvati 
attraverso gli `artifact`, ma unicamente nel caso in 
cui il job fallisca. Questa condizionalità è facilmente realizzata 
mediante l'uso del parametro `when` impostato `on_failure`. Questa scelta 
è stata presa poiché in caso di successo non si ritiene utile 
avere tali dati di log.

Con `Prospector` (tramite l'uso di `pylint`, `pep8` etc...) sono 
verificati: 

- errori
- potenziali problemi
- violazione delle convenzioni e degli standard
- complessità

Con `Bandit` viene invece fatta un'analisi statica del codice, con particolare
interesse per l'ambito della sicurezza.

Questo stage viene eseguito su tutti i branch. 

#### Unit test
È stata presa la decisione di utilizzare `pytest` in quanto è uno dei tool 
più utilizzati per i test in python. In questa fase sono testate le 
operazioni di add, sub, mul e div. 
Il job si occupa solamente di avviare `pytest` sui test programmati.
Anche in questo caso i log vengono salvati attraverso gli `artifact` 
solamente nell'eventualità in cui il job fallisca.

Questo stage viene eseguito su tutti i branch. 

#### Integration test
Anche in questo caso gli integration test sono eseguiti con `pytest`.
In questa fase sono testate la connessione al database remoto e le operazioni 
sullo stesso. Inoltre viene testato, tramite `requests`, il funzionamento dell'API. 
E nuovamente, anche in questo caso i log vengono salvati attraverso gli `artifact` 
solamente nell'eventualità in cui il job fallisca.

Questo stage viene eseguito su tutti i branch. 

#### Package
Non avendo motivo di creare un `wheel` per il progetto è stato scelto di
creare due archivi compressi che fossero eventualmente scaricabili ed
utilizzabili per potenziali test e/o verifiche.

Viene utilizzato `setuptools` per creare due diversi archivi: uno in formato 
`zip` e uno in formato `gztar`. Tali archivi sono salvati come `artifact` 
solo nell'eventualità in cui il job ha successo. Tale scelta poiché non sarebbe 
utile (o addirittura possibile) essere in possesso di questi file in caso 
di fallimento.

Questo stage viene eseguito solamente nei branch `develop` e `master`.
Si ritiene necessario creare l'archivio solo di questi branch 
in modo da rendere disponibile al download il codice solamente nelle sue 
versioni di produzione e di staging.

In questo stage non si ha più la necessità di usare l'ambiente `virtualenv` 
e quindi è applicata un'operazione di "overload" del `before_script` che 
sovrascrive quello globale con uno vuoto. La cache è quindi disabilitata. 

A partire da questo stage quest'ultima operazione è ripetuta per tutti i 
successivi in quanto le dipendenze descritte precedentemente non sono 
più necessarie.

#### Release
In questa fase ci si occupa di creare un'immagine Docker dell'applicativo, 
nonché di caricare la medesima sul `Gitlab Container Registry`.
La scelta dell'utilizzo di Docker è stata presa per comodità nell'utilizzo 
di questo ambiente, ma soprattutto in ottica delle successive modalità di 
deploy descritte nei seguenti paragrafi.

Questo stage ha come dipendenza, in qualità di ulteriore controllo, la 
fase di `package`.

Questo stage viene eseguito solamente nei branch `develop` e `master` per un
discorso analogo a quello fatto nello stage precedente.

#### Deploy

Dividiamo lo stage di deploy a seconda del branch:

- Per il branch `develop` si è scelto di effettuare il deploy 
  su una [VM Azure](http://20.56.8.72/) usata come server di `staging`.
  Per questa fase è stato usato uno script `bash` che si collega tramite 
  `ssh` alla macchina virtuale. Lo script si occupa di effettuare 
  il `pull` dell'immagine docker dal Gitlab Container Registry e di 
  avviare il relativo container. 
  La scelta di utilizzare Azure come server di staging è data dalla 
  praticità offerta dell'uso della VM e soprattutto dalla maggior velocità 
  nella fase di caricamento e avvio.
- Per il branch `master` si è scelto di effettuare il deploy 
  su [Heroku](http://unishare-assignment1.herokuapp.com/) che poi verrà 
  usato in `production`.
  Per questa fase si esegue il pull dell'immagine precedentemente caricata sul 
  Gitlab Container Registry in modo da poterne effettuare il re-tag utile 
  ad Heroku e quindi pusharla sul registry di Heroku. 
  Per completare il processo, tramite uno script, si procede alla release 
  dell'immagine appena pushata su Heroku tramite una chiamata `curl` alle loro API 
  ([https://devcenter.heroku.com/changelog-items/1426](https://devcenter.heroku.com/changelog-items/1426)).
  La scelta di utilizzare Heroku come server di production è data, oltre che dalla 
  gratuità del servizio, dalla possibilità di utilizzare un dominio 
  di secondo livello personalizzabile (vedi [unishare-assignment1.herokuapp.com](http://unishare-assignment1.herokuapp.com/)) per l'uso dell'API.

Al termine del deploy vero e proprio è stata inserita una verifica aggiuntiva 
per il controllo che l'applicativo sia correttamente online e che quindi la 
API sia realmente accessibile.
Per fare questo check si esegue una chiamata `curl`, dalla quale viene estratto 
il valore di `status` tramite `jq`. Una volta salvato tale risultato in una 
variabile si controlla che il suo valore sia "success": in caso negativo viene 
sollevato un errore che interromperebbe la pipeline.

Per questa verifica, nel caso di Azure, si è dovuto ricorrere ad un breve `sleep` 
aggiuntivo prima dell'esecuzione della chiamata `curl`. Quest workaround è 
con tutta probabilità necessario per permettere all'applicativo di avere qualche 
secondo per completare l'avvio.

La scelta di utilizzare sia Azure che Heroku è stata presa per sperimentare 
due tecniche di deployment differenti: l'uso di una VM e l'uso di un modello PaaS 
(come Heroku). Inoltre la doppia scelta ci permette di avere due deploy differenti, 
uno per la fase di staging e un altro per quella di production.

### Problematiche

Inizialmente abbiamo avuto problemi sui tempi di esecuzione della pipeline: 
tra le dipendenze era stato inserito `pandas` che impiegava molto tempo ad essere 
installato. 
Questo problema ci ha portato a terminare i minuti di pipeline offerti gratuitamente 
da Gitlab obbligandoci a trasferire il repository, che inizialmente si trovava al seguente link:
[https://gitlab.com/unishare/processo-e-sviluppo-del-software/assignment-1](https://gitlab.com/unishare/processo-e-sviluppo-del-software/assignment-1).

Un'ulteriore problematica riscontrata è stata riguardante `prospector`. 
L'esecuzione del tool presentava degli errori derivanti dalla scorretta 
rilevazione automatica del framework `django`, da noi non utilizzato.
Tale framework effettuava dei controlli utilizzando `pylint-django` che però 
non era installato poiché non presente nei nostri prerequisiti 
(perchè effettivamente non era necessario). 
A causa di questo, l'esecuzione di prospector mostrava un errore di _dipendenze non risolte_. 
Abbiamo risolto tale problema con l'utilizzo del flag `--no-autodetect` all'avvio 
del tool.

Anche una terza problematica è correlata all'utilizzo di `prospector`. 
È stato necessario sopprimere i _warning_ per la segnalazione di librerie deprecate.
Tale decisione, inevitabile, è stata presa in quanto la dipendenza `pymongo` 
utilizza dei metodi ormai deprecati di `dnspython`.

Una ultima segnalazione di criticità arriva da Heroku. In quest'ultima fase 
abbiamo avuto delle problematiche di autenticazione dovute ad alcuni cambiamenti 
architetturali poco documentati sul sito ufficiale. 